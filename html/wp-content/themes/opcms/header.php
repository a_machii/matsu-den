<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
<?php
  global $this_pagetitle;
  global $this_page_keywd;
  global $this_page_desc;
  global $this_page_slug;
  global $this_archive_title;
?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php if($this_pagetitle) : echo $this_pagetitle. ' | '; endif; ?>水中ポンプ、特殊モーター、減速機の修理・販売・メンテナンスは有限会社松本電機工業所</title>
    <meta name="description" content="<?php if($this_page_desc) : echo $this_page_desc. ' - '; elseif($this_pagetitle) : echo $this_pagetitle. ' - '; endif; ?>水中ポンプ、特殊モーター、減速機等の電動機の修理・販売・メンテナンスを行う栃木県宇都宮市の有限会社松本電機工業所" >
    <meta name="keywords" content="<?php if($this_page_keywd) : echo $this_page_keywd. ','; elseif($this_pagetitle) : echo $this_pagetitle. ','; endif; ?>水中ポンプ,モーター,特殊モーター,大型モーター,減速機,電動機,修理,販売,メンテナンス,コイル巻き" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no,address=no,email=no">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta property="og:title" content="<?php if($this_pagetitle) : echo $this_pagetitle. ' | '; endif; ?>水中ポンプ、特殊モーター、減速機の修理・販売・メンテナンスは有限会社松本電機工業所" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo home_url('/'); ?><?php if($this_page_slug) : echo $this_page_slug; elseif($this_archive_title) : echo $this_archive_title; endif; ?>">
    <meta property="og:image" content="http://www.matsu-den.co.jp/assets/img/ogp_img.jpg" />
    <meta property="og:site_name"  content="有限会社松本電機工業所" />
    <meta property="og:description" content="<?php if($this_page_desc) : echo $this_page_desc. ' - '; elseif($this_pagetitle) : echo $this_pagetitle. ' - '; endif; ?>水中ポンプ、特殊モーター、減速機等の電動機の修理・販売・メンテナンスを行う栃木県宇都宮市の有限会社松本電機工業所" />
    <link rel="stylesheet" href="<?php echo home_url('/'); ?>assets/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <!--&#91;if lt IE 9&#93;>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <!&#91;endif&#93;-->
<?php wp_head(); ?>
  </head>

  <body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.7";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <header>
<?php if(is_home()): ?>
      <div id="header">
<?php else: ?>
      <div id="header" class="low_header">
<?php endif; ?>
        <div class="inner">
          <h1 class="pc">水中ポンプ,特殊モーター,減速機等の電動機修理･販売･メンテナンスを行う有限会社松本電機工業所</h1>
          <h1 class="rsp left"><a href="<?php echo home_url('/'); ?>"><img src="<?php echo home_url('/'); ?>assets/img/sp/logo.png" width="290" height="44" alt="有限会社松本電機工業所"></a></h1>
          <p class="htel pc"><img src="<?php echo home_url('/'); ?>assets/img/htel.gif" width="194" height="33" alt="電話:028-658-1649"></p>
          <p class="hlogo left pc"><a href="<?php echo home_url('/'); ?>"><img src="<?php echo home_url('/'); ?>assets/img/logo.png" width="262" height="40" alt="有限会社松本電機工業所"></a></p>
          <p id="menu_btn" class="rsp right"><img src="<?php echo home_url('/'); ?>assets/img/sp/menu_btn.gif" alt="メニュー"></p>
          <p class="tel_btn rsp right"><a href="tel:028-658-1649"><img src="<?php echo home_url('/'); ?>assets/img/sp/tel_btn.gif" alt="電話：028-658-1649"></a></p>

          <nav>
            <ul id="gnav" class="right pc">
              <li><a href="<?php echo home_url('/'); ?>" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/gnav01.gif" width="78" height="19" alt="HOME"></a></li><!--
              --><li><a href="<?php echo home_url('/'); ?>company" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/gnav02.gif" width="85" height="19" alt="会社案内"></a></li><!--
              --><li><a href="<?php echo home_url('/'); ?>business" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/gnav03.gif" width="85" height="19" alt="事業内容"></a></li><!--
              --><li><a href="<?php echo home_url('/'); ?>facility" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/gnav04.gif" width="86" height="19" alt="設備紹介"></a></li><!--
              --><li><a href="<?php echo home_url('/'); ?>product" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/gnav05.gif" width="86" height="19" alt="製品紹介"></a></li><!--
              --><li><a href="<?php echo home_url('/'); ?>news" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/gnav06.gif" width="81" height="19" alt="お知らせ"></a></li>
            </ul>

            <ul id="menu" class="rsp">
              <li><a href="<?php echo home_url('/'); ?>">HOME</a></li>
              <li><a href="<?php echo home_url('/'); ?>company">会社案内</a></li>
              <li><a href="<?php echo home_url('/'); ?>business">事業内容</a></li>
              <li><a href="<?php echo home_url('/'); ?>facility">設備紹介</a></li>
              <li><a href="<?php echo home_url('/'); ?>product">製品紹介</a></li>
              <li><a href="<?php echo home_url('/'); ?>news">お知らせ</a></li>
            </ul>
          </nav>
        </div><!-- /.inner -->
      </div><!-- /#header -->

<?php if(is_home()): ?>
      <div id="mimg" class="pc">
        <ul class="slider">
          <li style="background-image:url('<?php echo home_url('/'); ?>assets/img/top/mimg01.jpg');"></li>
          <li style="background-image:url('<?php echo home_url('/'); ?>assets/img/top/mimg02.jpg');"></li>
          <li style="background-image:url('<?php echo home_url('/'); ?>assets/img/top/mimg03.jpg');"></li>
        </ul>
      </div><!--/.mimg-->

      <div id="rmimg" class="rsp">
        <ul class="rslider">
          <li><img src="<?php echo home_url('/'); ?>assets/img/sp/top/mimg01.jpg" width="720" height="399" alt=""></li>
          <li><img src="<?php echo home_url('/'); ?>assets/img/sp/top/mimg02.jpg" width="720" height="399" alt=""></li>
          <li><img src="<?php echo home_url('/'); ?>assets/img/sp/top/mimg03.jpg" width="720" height="399" alt=""></li>
        </ul>
      </div><!-- /#rmimg -->
<?php elseif(is_page()): ?>
      <nav id="breadcrumb" class="pc">
        <ol>
          <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
            <a itemprop="url" href="<?php echo home_url('/'); ?>"><span itemprop="title">ホーム&nbsp;&nbsp;</span></a>
          </li>
          <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
            <span itemprop="title">&gt;&nbsp;&nbsp;<?php echo $this_pagetitle; ?></span>
          </li>
        </ol>
      </nav>

      <div id="low_mimg">
        <div class="inner">
          <h2 class="h2ttl <?php echo $this_page_slug; ?>"><img src="<?php echo home_url('/'); ?>assets/img/<?php echo $this_page_slug; ?>/h2ttl.gif" alt="<?php echo $this_pagetitle; ?>"></h2>
        </div><!-- /.inner -->
      </div><!-- /#low_mimg -->

<?php elseif(is_post_type_archive(array('product','news','company'))): ?>
      <nav id="breadcrumb" class="pc">
        <ol>
          <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
            <a itemprop="url" href="<?php echo home_url('/'); ?>"><span itemprop="title">ホーム&nbsp;&nbsp;</span></a>
          </li>
          <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
            <span itemprop="title">&gt;&nbsp;&nbsp;<?php echo $this_pagetitle; ?></span>
          </li>
        </ol>
      </nav>

      <div id="low_mimg">
        <div class="inner">
          <h2 class="h2ttl <?php echo $this_archive_title; ?>"><img src="<?php echo home_url('/'); ?>assets/img/<?php echo$this_archive_title; ?>/h2ttl.gif" alt="<?php echo $this_archive_title; ?>"></h2>
        </div><!-- /.inner -->
      </div><!-- /#low_mimg -->
<?php endif; ?>
    </header>