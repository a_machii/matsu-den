<?php /* Template Name: page */ ?>
<?php
  $page_id = $post->ID;
  $content = get_page($page_id);
  $this_pagetitle = $content->post_title;
  $this_page_slug = $content->post_name;
?>
<?php get_header(); ?>

    <main id="<?php echo $this_page_slug; ?>" class="low_main">
      <div class="inner">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>

<?php if(is_page(array('business','facility'))): ?>
        <div id="bnr_area" class="pc">
          <div class="bnr">
            <p class="txt01 left">各種電動機のメンテナンスのプロフェッショナルです</p>
            <div class="txt_area01 right">
              <p class="txt02">修理のお見積りやご質問などお気軽にお問い合わせください。</p>
              <p class="txt03 bold"><span>Tel.</span>028-658-1649  <span>Fax.</span>028-659-2299</p>
            </div><!-- /txt_area -->
          </div><!-- /.bnr -->
          <div class="txt_area02">
            <p>種モーター・水中ポンプのメンテナンス・修理・販売ならおまかせ！</p>
            <p>各種モーター巻替、モーター回転制御改造、水中ポンプのメンテナンス・修理・販売を行っております。また高圧モーター等の特殊なものにも対応しております。取扱製品一覧に掲載されている製品は一例です。幅広く対応させて頂きますので、お気軽にお問合せ下さい。</p>
          </div><!-- /.txt_area02 -->
        </div><!-- /#bnr_area -->
<?php else: endif; ?>
      </div><!-- /.inner -->
    </main><!-- /#<?php echo $this_page_slug; ?> .low_main -->

<?php get_footer(); ?>