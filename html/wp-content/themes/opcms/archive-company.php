<?php $this_pagetitle = get_post_type_object(get_post_type())->label; ?>
<?php $this_archive_title = get_post_type_object(get_post_type())->name; ?>
<?php get_header(); ?>

    <main id="company" class="low_main">
      <div class="inner">
        <section id="section01" class="section">
          <h3 class="h3ttl01">
            <img src="<?php echo home_url('/'); ?>assets/img/company/h3ttl01.gif" width="63" height="21" alt="ご挨拶" class="pc">
            <img src="<?php echo home_url('/'); ?>assets/img/sp/company/h3ttl01.gif" width="86" height="29" alt="" class="rsp rh3ttl01"></h3>
          <div class="txt_area">
            <p>有限会社松本電機工業所のホームページをご覧いただきありがとうございます。</p>
            <p>弊社は創業以来栃木県宇都宮市を基点とし各種電動機、ポンプの修理を行ってまいりました。今後につきましても皆様方のニーズに応えるべく、社員一丸となり不断の努力を続けてまいる所存です。</p>
            <p class="txt">今後とも温かいご指導ご鞭撻のほどを宜しくお願い申し上げます。</p>
            <p class="signature right">
              <img src="<?php echo home_url('/'); ?>assets/img/company/signature.gif" width="187" height="17" alt="代表取締役 松本広孝" class="pc">
              <img src="<?php echo home_url('/'); ?>assets/img/sp/company/signature.gif" width="300" height="28" alt="代表取締役 松本広孝" class="rsp">
            </p>
          </div><!-- /.txt_area -->
        </section><!-- /#section01 -->

        <section id="section02" class="section">
          <h3 class="h3ttl02">
            <img src="<?php echo home_url('/'); ?>assets/img/company/h3ttl02.gif" width="88" height="21" alt="企業情報" class="pc">
            <img src="<?php echo home_url('/'); ?>assets/img/sp/company/h3ttl02.gif" width="120" height="29" alt="企業情報" class="rsp rh3ttl02">
          </h3>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
        </section><!-- /#section02 -->

        <section id="section03" class="section last">
          <h3 class="h3ttl02">
            <img src="<?php echo home_url('/'); ?>assets/img/company/h3ttl03.gif" width="130" height="21" alt="お問い合わせ" class="pc">
            <img src="<?php echo home_url('/'); ?>assets/img/sp/company/h3ttl03.gif" width="177" height="29" alt="お問い合わせ" class="rsp rh3ttl03">
          </h3>
          <p class="img"><img src="<?php echo home_url('/'); ?>assets/img/company/img01.gif" width="640" height="46" alt="お問い合わせいただきありがとうございます。"></p>
          <div class="cnt_bnr">
            <div class="txt_area left pc">
              <p>弊社の事業、製品、見積等に関するお問い合わせはお電話にて承っております。お気軽に問い合わせください。</p>
              <p>有限会社松本電機工業所　〒321-0101 栃木県宇都宮市江曽島本町9-1</p>
            </div><!-- /.txt_area -->
            <p class="txt right pc">受付時間：09：00～18：00（土・日・祝祭日を除く）</p>
            <p class="rsp">弊社の事業、製品、見積等に関するお問い合わせはお電話にて承っております。お気軽に問い合わせください。</p>
            <p class="tel rsp"><img src="<?php echo home_url('/'); ?>assets/img/sp/company/tel.gif" width="610" height="65" alt="電話:028-658-1649"></p>
            <p class="rsp">受付時間：09：00～18：00（土・日・祝祭日を除く）</p>
          </div><!-- /.cnt_bnr -->
        </section><!-- /#section03 -->
      </div><!-- /.inner -->
    </main>

<?php get_footer(); ?>