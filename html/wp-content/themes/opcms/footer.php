    <footer id="footer">
      <div class="f_inner pc">
        <dl class="left">
          <dt><img src="<?php echo home_url('/'); ?>assets/img/logo.png" width="262" height="40" alt="有限会社松本電機工業所"></dt>
          <dd>〒321-0101</dd>
          <dd>栃木県宇都宮市江曽島本町9-1</dd>
          <dd>TEL 028-658-1649／FAX 028-659-2299</dd>
        </dl>

        <nav>
          <ul class="right">
            <li><a href="<?php echo home_url('/'); ?>">HOME</a></li>
            <li><a href="<?php echo home_url('/'); ?>company">会社案内</a></li>
            <li><a href="<?php echo home_url('/'); ?>business">事業内容</a></li>
            <li><a href="<?php echo home_url('/'); ?>facility">設備紹介</a></li>
            <li><a href="<?php echo home_url('/'); ?>product">製品紹介</a></li>
            <li><a href="<?php echo home_url('/'); ?>news">お知らせ</a></li>
          </ul>
        </nav>
        <p class="to_top"><a href="#header" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/tt_btn.gif" width="40" height="40" alt=""></a></p>
        <p class="cr">Copyroght(C)有限会社松本電機工業所.All Rights Reserved.</p>
      </div><!-- /.f_inner -->

      <p class="rto_top bold rsp"><a href="#header" class="ophv">PAGE TOP</a></p>

      <nav class="rsp">
        <ul class="fnav">
          <li><a href="<?php echo home_url('/'); ?>"><img src="<?php echo home_url('/'); ?>assets/img/sp/fnav01.jpg" width="360" height="93" alt="HOME"></a></li>
          <li><a href="<?php echo home_url('/'); ?>company"><img src="<?php echo home_url('/'); ?>assets/img/sp/fnav02.jpg" width="360" height="93" alt="会社案内"></a></li>
          <li><a href="<?php echo home_url('/'); ?>business"><img src="<?php echo home_url('/'); ?>assets/img/sp/fnav03.jpg" width="360" height="93" alt="事業内容"></a></li>
          <li><a href="<?php echo home_url('/'); ?>facility"><img src="<?php echo home_url('/'); ?>assets/img/sp/fnav04.jpg" width="360" height="93" alt="設備紹介"></a></li>
          <li><a href="<?php echo home_url('/'); ?>product"><img src="<?php echo home_url('/'); ?>assets/img/sp/fnav05.jpg" width="360" height="95" alt="製品紹介"></a></li>
          <li><a href="<?php echo home_url('/'); ?>news"><img src="<?php echo home_url('/'); ?>assets/img/sp/fnav06.jpg" width="360" height="95" alt="お知らせ"></a></li>
        </ul>
      </nav>

      <div class="address rsp">
        <dl class="address_in">
          <dt>有限会社 松本電機工業</dt>
          <dd class="txt">〒321-0101 栃木県宇都宮市江曽島本町9-1</dd>
          <dd>TEL 028-658-1649／FAX 028-659-229</dd>
        </dl>
      </div><!-- /.address -->

      <div class="switch rsp">
        <ul class="switch_in">
          <li><a href="#" id="btn_pc">PC</a></li>
          <li><a href="#" id="btn_rsp">モバイル</a></li>
        </ul>
      </div><!-- /.switch -->

      <p class="rcr rsp">Copyright © Koa/acacia. All Rights Reserved.</p>
    </footer>

    <!-- script -->
    <script src="<?php echo home_url('/'); ?>assets/js/rollover/rollover.js" type="text/javascript"></script>
    <script src="<?php echo home_url('/'); ?>assets/js/rollover/opacity-rollover2.1.js" type="text/javascript"></script>
    <script src="<?php echo home_url('/'); ?>assets/js/smoothScrollEx.js" type="text/javascript"></script>
    <script src="<?php echo home_url('/'); ?>assets/js/jquery.bxslider.min.js" type="text/javascript"></script>
    <script src="<?php echo home_url('/'); ?>assets/js/jquery.cookie.js" type="text/javascript"></script>

    <!-- ロールオーバー -->
    <script type="text/javascript">
      $(document).ready(function() {
        $('.ophv').opOver(1.0, 0.6, 200, 200);
      });
    </script>

<?php if(is_home()): ?>
    <!-- スライダー -->
    <script type="text/javascript">
      $(document).ready(function(){
        $('.slider').bxSlider({
          mode: 'fade', //スライドショーの種類
          auto: true, //自動再生
          controls: true, //prev/nextを非表示
          prevText: '<',
          nextText: '>',
          pager: false,
          speed: 8000, //スライドショーのスピード
          pause: 4000, //静止時間
        });
      });
    </script>

    <script type="text/javascript">
      $(document).ready(function(){
        $('.rslider').bxSlider({
          mode: 'fade', //スライドショーの種類
          auto: true, //自動再生
          controls: false, //prev/nextを非表示
          pager: false,
          speed: 8000, //スライドショーのスピード
          pause: 4000, //静止時間
        });
      });
    </script>
<?php endif; ?>

    <!-- レスポンシブメニューボタン -->
    <script type="text/javascript">
      $(document).ready(function(){
        $(window).resize(function() {
          var w = $(window).width();
            if(750 <= w){ //ここにブレイクポイントの数値を入れて条件分岐
              $('#menu').css("display","none");
            }
          });
        $('#menu_btn').on('click',function(){
          $('#menu').slideToggle();
        });
      });
    </script>

    <!-- ヘッダーレスポンシブ -->
    <script>
      $(document).ready(function(){
        $(window).on('load resize',function(){
          var w = $(window).width();
          var x = 750;
          var header_h = w * 0.1527;
          var mb_w = w * 0.1375;
          var mb_p1 = header_h * 0.336;
          var mb_p2 = mb_w * 0.27;
          var tb_p = mb_w * 0.33;
          var mbimg_w = mb_w * 0.46;
          var tbimg_w = mb_w * 0.34;
          var img_h = header_h * 0.327;
          if (w <= x) {
            $('#menu_btn').css({
              'padding-top': mb_p1,
              'padding-right': mb_p2,
              'padding-bottom': mb_p1,
              'padding-left': mb_p2
            });
            $('#menu_btn img').css({
              width: mbimg_w,
              height: img_h
            });
            $('#menu').css({
              top: header_h
            });
            $('.tel_btn').css({
              'padding-top': mb_p1,
              'padding-right': tb_p,
              'padding-bottom': mb_p1,
              'padding-left': tb_p
            });
            $('.tel_btn img').css({
              width: tbimg_w,
              height: img_h
            })
          } else {}
        });
      });
    </script>

    <!-- pc/レスポンシブ切り替えボタン -->
    <script>
      $(document).ready(function() {
        $("head").append("<meta name='viewport' content="
          +($.cookie("switchScreen") == 1 ?
            "'width=980'" :
            "'width=device-width, initial-scale=1, maximum-scale=1'")
          +" />");

        $("#btn_pc, #btn_rsp").click(function() {
          $.cookie("switchScreen", $(this).attr("id") == "btn_pc" ? 1 : 0);
          location.reload();
          return false;
        });
      });
    </script>
<?php wp_footer(); ?>
  </body>
</html>