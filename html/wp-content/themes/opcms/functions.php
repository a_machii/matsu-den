<?php

function call_thissiteurl() {
    return site_url('/');
}
add_shortcode('thissiteurl', 'call_thissiteurl');

function remove_footer_admin () {
  echo 'お問い合わせは<a href="http://www.officepartner.jp/contact/" target="_blank">オフィスパートナー株式会社</a>まで';
}
add_filter('admin_footer_text', 'remove_footer_admin');

if (!current_user_can('administrator')) {
  add_filter('pre_site_transient_update_core', create_function('$a', "return null;"));
}

if (!current_user_can('edit_users')) {
  function remove_menus () {
    global $menu;
    $restricted = array(
      __('リンク'),
      __('ツール'),
      __('コメント'),
      __('プロフィール')
      );
    end ($menu);
    while (prev($menu)){
      $value = explode(' ',$menu[key($menu)][0]);
      if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){
        unset($menu[key($menu)]);
      }
    }
  }
  add_action('admin_menu', 'remove_menus');
}

function example_remove_dashboard_widgets() {
  global $wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // 現在の状況
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // 最近のコメント
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // 被リンク
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // プラグイン
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // クイック投稿
  //unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // 最近の下書き
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPressブログ
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // WordPressフォーラム
}
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets');

add_action( 'wp_before_admin_bar_render', 'hide_before_admin_bar_render' );
function hide_before_admin_bar_render() {
  global $wp_admin_bar;
  $wp_admin_bar->remove_menu( 'wp-logo' );
}

// ウィジェット
//register_sidebar();

/* ログイン画面のロゴ変更
------------------------------------------------------------*/
function my_custom_login_logo() {
  echo '<style type="text/css">
  h1 a { background-image:url('.get_bloginfo('template_directory').'/images/logo-login.png) !important; }</style>';
  echo '
  <script type="text/javascript">

  </script>
  ';
}
add_action('login_head', 'my_custom_login_logo');

/* メニューの表示順番
------------------------------------------------------------*/
function custom_menu_order($menu_ord) {
  if (!$menu_ord) return true;
  return array(
    'index.php', // ダッシュボード
    'separator1', // 最初の区切り線
    'edit.php', // 投稿
    'edit.php?post_type=page', // 固定ページ
    'upload.php', // メディア
    'link-manager.php', // リンク
    'edit-comments.php', // コメント
    'separator2', // 二つ目の区切り線
    'themes.php', // 外観
    'plugins.php', // プラグイン
    'users.php', // ユーザー
    'tools.php', // ツール
    'options-general.php', // 設定
    'separator-last', // 最後の区切り線
  );
}
add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order');

/* メニューを非表示にする（管理者以外）
------------------------------------------------------------*/
function remove_menus02 () {
  if (!current_user_can('level_10')) { //level10以下のユーザーの場合メニューをunsetする
    remove_menu_page('wpcf7'); //Contact Form 7
    global $menu;
    unset($menu[5]); // 投稿
    unset($menu[20]); // 固定ページ
  }
}
add_action('admin_menu', 'remove_menus02');

/* wp_head()のいらないタグを削除
-------------------------------------------------------------*/
// 絵文字
function disable_emoji() {
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'wp_shortlink_wp_head');
  remove_action('wp_head', 'rel_canonical');
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
add_action( 'init', 'disable_emoji' );

// Embed
remove_action('wp_head','rest_output_link_wp_head');
remove_action('wp_head','wp_oembed_add_discovery_links');
remove_action('wp_head','wp_oembed_add_host_js');

// generator
remove_action('wp_head', 'wp_generator');

// EditURI
remove_action('wp_head', 'rsd_link');

// wlwmanifest
remove_action('wp_head', 'wlwmanifest_link');

/* 固定ページではビジュアルエディタを利用できないようにする
------------------------------------------------------------*/
function disable_visual_editor_in_page(){
  global $typenow;
  if( $typenow == 'page' ){
    add_filter('user_can_richedit', 'disable_visual_editor_filter');
  }
}

function disable_visual_editor_filter(){
  return false;
}
add_action( 'load-post.php', 'disable_visual_editor_in_page' );
add_action( 'load-post-new.php', 'disable_visual_editor_in_page' );

/* 自動整形を無効にする
-------------------------------------------------------------*/
add_filter('the_content', 'wpautop_filter', 9);
function wpautop_filter($content) {
  global $post;
  $remove_filter = false;
    $arr_types = array('page'); //自動整形を無効にする投稿タイプを記述
    $post_type = get_post_type( $post->ID );
    if (in_array($post_type, $arr_types)) $remove_filter = true;
    if ( $remove_filter ) {
      remove_filter('the_content', 'wpautop');
      remove_filter('the_excerpt', 'wpautop');
    }
  return $content;
}

//＜p＞の自動挿入は残しつつ、HTMLソースが勝手に消されるのを止める
add_action('init', function() {
  remove_filter('the_title', 'wptexturize');
  remove_filter('the_content', 'wptexturize');
  remove_filter('the_excerpt', 'wptexturize');
  remove_filter('the_title', 'wpautop');
  remove_filter('the_content', 'wpautop');
  remove_filter('the_excerpt', 'wpautop');
  remove_filter('the_editor_content', 'wp_richedit_pre');
});

add_filter('tiny_mce_before_init', function($init) {
  $init['wpautop'] = false;
  $init['apply_source_formatting'] = ture;
  return $init;
});

/* カテゴリーの階層構造を正しく表示
------------------------------------------------------------*/
function lig_wp_category_terms_checklist_no_top( $args, $post_id = null ) {
  $args['checked_ontop'] = false;
  return $args;
}
add_action( 'wp_terms_checklist_args', 'lig_wp_category_terms_checklist_no_top' );

/* カスタム投稿の追加
------------------------------------------------------------*/
add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type('product',
    array(
      'label' => '製品紹介',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => true,
      'query_var' => false,
      'has_archive' => true,
      'exclude_from_search' => false,
      'menu_position' => 20,
      'supports' => array('title'),
      'taxonomies' => array('productcategory'),
      'labels' => array (
        'name' => '製品紹介',
        'all_items' => '製品紹介'
      )
    )
  );

  register_taxonomy(
    'productcategory',
    'product',
    array(
      'hierarchical' => true,
      'label' => 'カテゴリ',
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'product'),
      'singular_label' => 'カテゴリ'
    )
  );

  register_post_type('news',
    array(
      'label' => 'お知らせ',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => true,
      'query_var' => false,
      'has_archive' => true,
      'exclude_from_search' => false,
      'menu_position' => 20,
      'supports' => array('title','editor'),
      'taxonomies' => array('newscategory'),
      'labels' => array (
        'name' => 'お知らせ',
        'all_items' => 'お知らせ'
      )
    )
  );

  register_taxonomy(
    'newscategory',
    'news',
    array(
      'hierarchical' => true,
      'label' => 'カテゴリ',
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'news'),
      'singular_label' => 'カテゴリ'
    )
  );

  register_post_type('company',
    array(
      'label' => '会社案内',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => true,
      'query_var' => false,
      'has_archive' => true,
      'exclude_from_search' => false,
      'menu_position' => 20,
      'supports' => array('title','editor'),
      'taxonomies' => array('companycategory'),
      'labels' => array (
        'name' => '会社案内',
        'all_items' => '会社案内'
      )
    )
  );

  register_taxonomy(
    'companycategory',
    'company',
    array(
      'hierarchical' => true,
      'label' => 'カテゴリ',
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'company'),
      'singular_label' => 'カテゴリ'
    )
  );
}

/* カスタム分類アーカイブ用のリライトルールを追加する
------------------------------------------------------------*/
add_rewrite_rule('product/([^/]+)/page/([0-9]+)/?$', 'index.php?productcategory=$matches[1]&paged=$matches[2]', 'top');
add_rewrite_rule('news/([^/]+)/page/([0-9]+)/?$', 'index.php?newscategory=$matches[1]&paged=$matches[2]', 'top');
add_rewrite_rule('company/([^/]+)/page/([0-9]+)/?$', 'index.php?companycategory=$matches[1]&paged=$matches[2]', 'top');

/* pre get posts 設定
------------------------------------------------------------*/
function customize_main_query($query) {
  if ( is_admin() || ! $query->is_main_query() )
    return;

  if ( $query->is_post_type_archive( 'product' ) ) {
    $query->set( 'posts_per_page', '5' );
  }
  if ( $query->is_post_type_archive( 'news' ) ) {
    $query->set( 'posts_per_page', '5' );
  }
  if ( $query->is_home() ) {
    $query->set( 'post_type', 'news' );
    $query->set( 'posts_per_page', '5' );
  }
}
add_action( 'pre_get_posts', 'customize_main_query' );

/* 画像サイズの変更
-------------------------------------------------------------*/
add_theme_support('post-thumbnails');
if ( function_exists( 'add_image_size' ) ) {
  add_image_size( 'product_custom_size', 315, 231, true );
}

/* pagenation
-------------------------------------------------------------*/
function pagination($pages = '', $range = 9){
  // $showitems数から何ページ分ページネーションを作成するか計算する
  $showitems = 1;    //($range * 2)+1;

  // 現在いる記事一覧ページの値を取得
  global $paged;
  if(empty($paged)) $paged = 1;

  // 存在する記事に対するページ総数の判断
  if($pages == ''){
    global $wp_query;
    $pages = $wp_query->max_num_pages;
      if(!$pages){
        $pages = 1;
       }
     }

  // $pagesが1で無かった場合＝記事がある程度存在する場合のページネーションの表示
  if(1 != $pages){
    echo "<div class=\"pager_area\">\n\t<ul>\n\t\t<li class=\"pc\">Page ".$paged." of ".$pages."</li>\n";
    // 一つ前に戻るボタン
    if($paged > 1 && $showitems < $pages) echo "\t\t<li class=\"pc\"><a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a></li>\t\t<li class=\"rsp prev_btn\"><a href='".get_pagenum_link($paged - 1)."'>PREV</a></li>";

    // 現在のページ ループ部分
    for ($i=1; $i <= $pages; $i++){ //全記事一覧が1ページ以上ある
      if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
        echo ($paged == $i)? "\t\t<li class=\"active pc\"><span>".$i."</span></li>\n":"\t\t<li class=\"pc\"><a href='".get_pagenum_link($i)."'>".$i."</a></li>\n";
      }
    }

    // 一つ進むボタン
    if ($paged < $pages && $showitems < $pages) echo "\t\t<li class=\"pc\"><a href=\"".get_pagenum_link($paged + 1)."\">&rsaquo;</a></li>\t\t<li class=\"rsp next_btn\"><a href=\"".get_pagenum_link($paged + 1)."\">NEXT</a></li>\n";
    echo "\t</ul>\n</div><!-- /.pager_area -->\n";
  }
}

/* アーカイブ表記に「年」を追加
-------------------------------------------------------------*/
function my_archives_link($html){
  if(preg_match('/[0-9]+?<\/a>/', $html))
    $html = preg_replace('/([0-9]+?)<\/a>/', '$1年</a>', $html);
  if(preg_match('/title=[\'\"][0-9]+?[\'\"]/', $html))
    $html = preg_replace('/(title=[\'\"][0-9]+?)([\'\"])/', '$1年$2', $html);
  return $html;
}
add_filter('get_archives_link', 'my_archives_link', 10);

/* カスタム投稿タイプ用カレンダー
-------------------------------------------------------------*/
function get_cpt_calendar($cpt, $initial = true, $echo = true) {
  global $wpdb, $m, $monthnum, $year, $wp_locale, $posts;

  $cache = array();
  $key = md5( $m . $monthnum . $year );
  if ( $cache = wp_cache_get( 'get_calendar', 'calendar' ) ) {
    if ( is_array($cache) && isset( $cache[ $key ] ) ) {
      if ( $echo ) {
        echo apply_filters( 'get_calendar',  $cache[$key] );
        return;
      } else {
        return apply_filters( 'get_calendar',  $cache[$key] );
      }
    }
  }

  if ( !is_array($cache) )
  $cache = array();

  // Quick check. If we have no posts at all, abort!
  if ( !$posts ) {
    $gotsome = $wpdb->get_var("SELECT 1 as test FROM $wpdb->posts WHERE post_type = '$cpt' AND post_status = 'publish' LIMIT 1");
    if ( !$gotsome ) {
      $cache[ $key ] = '';
      wp_cache_set( 'get_calendar', $cache, 'calendar' );
      return;
    }
  }

  if ( isset($_GET['w']) )
  $w = ''.intval($_GET['w']);

  // week_begins = 0 stands for Sunday
  $week_begins = intval(get_option('start_of_week'));

  // Let's figure out when we are
  if ( !empty($monthnum) && !empty($year) ) {
    $thismonth = ''.zeroise(intval($monthnum), 2);
    $thisyear = ''.intval($year);
  } elseif ( !empty($w) ) {
    // We need to get the month from MySQL
    $thisyear = ''.intval(substr($m, 0, 4));
    $d = (($w - 1) * 7) + 6; //it seems MySQL's weeks disagree with PHP's
    $thismonth = $wpdb->get_var("SELECT DATE_FORMAT((DATE_ADD('{$thisyear}0101', INTERVAL $d DAY) ), '%m')");
  } elseif ( !empty($m) ) {
    $thisyear = ''.intval(substr($m, 0, 4));
    if ( strlen($m) < 6 )
      $thismonth = '01';
    else
      $thismonth = ''.zeroise(intval(substr($m, 4, 2)), 2);
  } else {
      $thisyear = gmdate('Y', current_time('timestamp'));
      $thismonth = gmdate('m', current_time('timestamp'));
  }

  $unixmonth = mktime(0, 0 , 0, $thismonth, 1, $thisyear);
  $last_day = date('t', $unixmonth);

  // Get the next and previous month and year with at least one post
  $previous = $wpdb->get_row("SELECT MONTH(post_date) AS month, YEAR(post_date) AS year
    FROM $wpdb->posts
    WHERE post_date < '$thisyear-$thismonth-01'
    AND post_type = '$cpt' AND post_status = 'publish'
      ORDER BY post_date DESC
      LIMIT 1");
  $next = $wpdb->get_row("SELECT MONTH(post_date) AS month, YEAR(post_date) AS year
    FROM $wpdb->posts
    WHERE post_date > '$thisyear-$thismonth-{$last_day} 23:59:59'
    AND post_type = '$cpt' AND post_status = 'publish'
      ORDER BY post_date ASC
      LIMIT 1");

  /* translators: Calendar caption: 1: month name, 2: 4-digit year */
  $calendar_caption = _x('%1$s %2$s', 'calendar caption');
  $calendar_output = '<table id="wp-calendar">
  <caption>' . sprintf($calendar_caption, $wp_locale->get_month($thismonth), date('Y', $unixmonth)) . '</caption>
  <thead>
  <tr>';

  $myweek = array();

  for ( $wdcount=0; $wdcount<=6; $wdcount++ ) {
    $myweek[] = $wp_locale->get_weekday(($wdcount+$week_begins)%7);
  }

  foreach ( $myweek as $wd ) {
    $day_name = (true == $initial) ? $wp_locale->get_weekday_initial($wd) : $wp_locale->get_weekday_abbrev($wd);
    $wd = esc_attr($wd);
    $calendar_output .= "\n\t\t<th scope=\"col\" title=\"$wd\">$day_name</th>";
  }

  $calendar_output .= '
  </tr>
  </thead>

  <tfoot>
  <tr>';

  if ( $previous ) {
    $calendar_output .= "\n\t\t".'<td colspan="3" id="prev"><a href="' . get_month_link($previous->year, $previous->month) . '?post_type='.$cpt.'" title="' . esc_attr( sprintf(__('View posts for %1$s %2$s'), $wp_locale->get_month($previous->month), date('Y', mktime(0, 0 , 0, $previous->month, 1, $previous->year)))) . '">&laquo; ' . $wp_locale->get_month_abbrev($wp_locale->get_month($previous->month)) . '</a></td>';
  } else {
    $calendar_output .= "\n\t\t".'<td colspan="3" id="prev" class="pad">&nbsp;</td>';
  }

  $calendar_output .= "\n\t\t".'<td class="pad">&nbsp;</td>';

  if ( $next ) {
    $calendar_output .= "\n\t\t".'<td colspan="3" id="next"><a href="' . get_month_link($next->year, $next->month) . '?post_type='.$cpt.'" title="' . esc_attr( sprintf(__('View posts for %1$s %2$s'), $wp_locale->get_month($next->month), date('Y', mktime(0, 0 , 0, $next->month, 1, $next->year))) ) . '">' . $wp_locale->get_month_abbrev($wp_locale->get_month($next->month)) . ' &raquo;</a></td>';
  } else {
    $calendar_output .= "\n\t\t".'<td colspan="3" id="next" class="pad">&nbsp;</td>';
  }

  $calendar_output .= '
  </tr>
  </tfoot>

  <tbody>
  <tr>';

  // Get days with posts
  $dayswithposts = $wpdb->get_results("SELECT DISTINCT DAYOFMONTH(post_date)
    FROM $wpdb->posts WHERE post_date >= '{$thisyear}-{$thismonth}-01 00:00:00'
    AND post_type = '$cpt' AND post_status = 'publish'
    AND post_date <= '{$thisyear}-{$thismonth}-{$last_day} 23:59:59'", ARRAY_N);
  if ( $dayswithposts ) {
    foreach ( (array) $dayswithposts as $daywith ) {
      $daywithpost[] = $daywith[0];
    }
  } else {
    $daywithpost = array();
  }

  if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false || stripos($_SERVER['HTTP_USER_AGENT'], 'camino') !== false || stripos($_SERVER['HTTP_USER_AGENT'], 'safari') !== false)
    $ak_title_separator = "\n";
  else
    $ak_title_separator = ', ';

    $ak_titles_for_day = array();
    $ak_post_titles = $wpdb->get_results("SELECT ID, post_title, DAYOFMONTH(post_date) as dom "
      ."FROM $wpdb->posts "
      ."WHERE post_date >= '{$thisyear}-{$thismonth}-01 00:00:00' "
      ."AND post_date <= '{$thisyear}-{$thismonth}-{$last_day} 23:59:59' "
      ."AND post_type = '$cpt' AND post_status = 'publish'"
  );
  if ( $ak_post_titles ) {
    foreach ( (array) $ak_post_titles as $ak_post_title ) {

      /** This filter is documented in wp-includes/post-template.php */
      $post_title = esc_attr( apply_filters( 'the_title', $ak_post_title->post_title, $ak_post_title->ID ) );

      if ( empty($ak_titles_for_day['day_'.$ak_post_title->dom]) )
        $ak_titles_for_day['day_'.$ak_post_title->dom] = '';
      if ( empty($ak_titles_for_day["$ak_post_title->dom"]) ) // first one
        $ak_titles_for_day["$ak_post_title->dom"] = $post_title;
      else
        $ak_titles_for_day["$ak_post_title->dom"] .= $ak_title_separator . $post_title;
    }
  }

  // See how much we should pad in the beginning
  $pad = calendar_week_mod(date('w', $unixmonth)-$week_begins);
  if ( 0 != $pad )
    $calendar_output .= "\n\t\t".'<td colspan="'. esc_attr($pad) .'" class="pad">&nbsp;</td>';

  $daysinmonth = intval(date('t', $unixmonth));
  for ( $day = 1; $day <= $daysinmonth; ++$day ) {
    if ( isset($newrow) && $newrow )
      $calendar_output .= "\n\t</tr>\n\t<tr>\n\t\t";
    $newrow = false;

    if ( $day == gmdate('j', current_time('timestamp')) && $thismonth == gmdate('m', current_time('timestamp')) && $thisyear == gmdate('Y', current_time('timestamp')) )
      $calendar_output .= '<td id="today">';
    else
      $calendar_output .= '<td>';

    if ( in_array($day, $daywithpost) ) // any posts today?
      $calendar_output .= '<a href="' . get_day_link( $thisyear, $thismonth, $day ) . '?post_type='.$cpt.'" title="' . esc_attr( $ak_titles_for_day[ $day ] ) . "\">$day</a>";
    else
      $calendar_output .= $day;
    $calendar_output .= '</td>';

    if ( 6 == calendar_week_mod(date('w', mktime(0, 0 , 0, $thismonth, $day, $thisyear))-$week_begins) )
      $newrow = true;
  }

  $pad = 7 - calendar_week_mod(date('w', mktime(0, 0 , 0, $thismonth, $day, $thisyear))-$week_begins);
  if ( $pad != 0 && $pad != 7 )
    $calendar_output .= "\n\t\t".'<td class="pad" colspan="'. esc_attr($pad) .'">&nbsp;</td>';

  $calendar_output .= "\n\t</tr>\n\t</tbody>\n\t</table>";

  $cache[ $key ] = $calendar_output;
  wp_cache_set( 'get_calendar', $cache, 'calendar' );

  if ( $echo )
    echo apply_filters( 'get_calendar',  $calendar_output );
  else
    return apply_filters( 'get_calendar',  $calendar_output );
}

?>