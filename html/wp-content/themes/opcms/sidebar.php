<?php $myptype = $post->post_type; ?>
        <asice id="sidebar" class="right pc">
          <dl class="post">
            <dt>最近の投稿</dt>
<?php
  $args = array (
    'posts_per_page' => 5,
    'post_type' => 'news',
  );
  $my_posts = get_posts( $args ); // $posts = とは書かない
  global $post; // テンプレートファイル内なら書かなくても良い
  $count_post = 0;
  foreach ( $my_posts as $post ) : //$postにしないとthe_titleが取得できない
  setup_postdata( $post );
  $postid = get_the_ID();
?>
            <dd><a href="<?php echo home_url('/'); ?>news#post<?php echo $postid; ?>"><?php the_title(); ?></a></dd>
<?php
  endforeach;
  wp_reset_postdata();
?>
          </dl>

          <div class="calendar">
            <p>カレンダー</p>
            <?php get_cpt_calendar('news'); ?>
          </div><!-- /.calendar -->
        </aside><!-- /#sidebar -->