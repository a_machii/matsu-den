<?php get_header(); ?>

    <main id="top">
      <section id="section01" class="section">
        <div class="inner">
          <h2 class="pc"><img src="<?php echo home_url('/'); ?>assets/img/top/sec01_ttl.gif" width="322" height="27" alt="確かな技術と実績の融合"></h2>
          <h2 class="rh2ttl rsp"><img src="<?php echo home_url('/'); ?>assets/img/sp/top/sec01_ttl.gif" width="446" height="35" alt="確かな技術と実績の融合"></h2>
          <ul>
            <li>
              <section>
                <p class="img">
                  <img src="<?php echo home_url('/'); ?>assets/img/top/img01.jpg" width="300" height="129" alt="修理の写真" class="pc">
                  <img src="<?php echo home_url('/'); ?>assets/img/sp/top/img01.jpg" width="680" height="315" alt="修理の写真" class="rsp">
                </p>
                <h3 class="pc"><img src="<?php echo home_url('/'); ?>assets/img/top/unit_ttl01.gif" width="280" height="40" alt="小回りの利く修理のスペシャリスト"></h3>
                <h3 class="h3ttl01 rsp"><img src="<?php echo home_url('/'); ?>assets/img/sp/top/unit_ttl01.gif" width="497" height="64" alt="小回りの利く修理のスペシャリスト" class="rsp"></h3>
                <div class="txt_area">
                  <p>当社は、各種電動機、モーター、ポンプなど低圧モーター 専門での修理を主な業務としております。</p>
                  <p>焼損してしまう前に、オーバーホールなどのメンテナンスをおすすめします。</p>
                </div><!-- /.txt_area -->
                <p class="pc"><a href="<?php echo home_url('/'); ?>business" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/td_btn.gif" width="89" height="21" alt="詳細はこちら"></a></p>
              </section>
            </li>
            <li>
              <section>
                <p class="img">
                  <img src="<?php echo home_url('/'); ?>assets/img/top/img02.jpg" width="300" height="129" alt="生産設備の写真" class="pc">
                  <img src="<?php echo home_url('/'); ?>assets/img/sp/top/img02.jpg" width="680" height="315" alt="技術力を支える充実の生産設備" class="rsp">
                </p>
                <h3 class="pc"><img src="<?php echo home_url('/'); ?>assets/img/top/unit_ttl02.gif" width="252" height="39" alt="技術力を支える充実の生産設備"></h3>
                <h3 class="h3ttl02 rsp"><img src="<?php echo home_url('/'); ?>assets/img/sp/top/unit_ttl02.gif" width="447" height="64" alt="技術力を支える充実の生産設備"></h3>
                <div class="txt_area">
                  <p>充実の設備でモーター・水中ポンプ・減速機などの修理、オーバーホールからメンテナンスまで、行います。</p>
                </div><!-- /.txt_area -->
                <p class="pc"><a href="<?php echo home_url('/'); ?>facility" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/td_btn.gif" width="89" height="21" alt="詳細はこちら"></a></p>
              </section>
            </li>
            <li>
              <section>
                <p class="img">
                  <img src="<?php echo home_url('/'); ?>assets/img/top/img03.jpg" width="300" height="129" alt="修理の写真" class="pc">
                  <img src="<?php echo home_url('/'); ?>assets/img/sp/top/img03.jpg" width="680" height="315" alt="修理の写真" class="rsp">
                </p>
                <h3 class="pc"><img src="<?php echo home_url('/'); ?>assets/img/top/unit_ttl03.gif" width="251" height="40" alt="高度な技術に裏打ちされた修理"></h3>
                <h3 class="h3ttl03 rsp"><img src="<?php echo home_url('/'); ?>assets/img/sp/top/unit_ttl03.gif" width="446" height="64" alt="高度な技術に裏打ちされた修理"></h3>
                <div class="txt_area">
                  <p>●水中ポンプ、特殊モーター、大型モーター</p>
                  <p>●減速機</p>
                  <p class="txt">●クレーンモーターのコイルの巻きかえオーバーホールを承っております。</p>
                </div><!-- /.txt_area -->
                <p class="pc"><a href="<?php echo home_url('/'); ?>product" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/td_btn.gif" width="89" height="21" alt="詳細はこちら"></a></p>
              </section><!-- /.unit -->
            </li>
          </ul>
        </div><!-- /.inner -->
      </section><!-- /#section01 -->

      <section id="section02" class="section">
        <div class="inner">
          <h2 class="h2ttl pc"><img src="<?php echo home_url('/'); ?>assets/img/top/sec02_ttl.gif" width="143" height="26" alt="お知らせ"><span><a href="<?php echo home_url('/'); ?>news">お知らせ一覧</a></span></h2>
          <div class="ttl_wrap rsp">
            <h2 class="rh2ttl"><img src="<?php echo home_url('/'); ?>assets/img/sp/top/sec02_ttl.gif" width="185" height="32" alt="お知らせ"></h2>
          </div><!-- /.ttl_wrap -->
          <ul>

<?php if(have_posts()): while(have_posts()): the_post(); ?>
  <?php $postid = get_the_ID(); ?>
            <li>
              <article>
                <h3 class="right"><a href="<?php echo home_url('/'); ?>news#post<?php echo $postid; ?>"><?php the_title(); ?></a></h3>
                <p class="date left"><?php the_time('Y年m月j日'); ?></p>
              </article>
            </li>
<?php endwhile; endif; ?>
          </ul>
        </div><!-- /.inner -->
      </section><!-- /#section02 -->

      <div id="section03" class="section">
        <div class="inner">
          <ul class="pc">
            <li><a href="<?php echo home_url('/'); ?>company" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/t_about.jpg" width="298" height="213" alt="松本電機工業所について"></a></li>
            <li><a href="<?php echo home_url('/'); ?>company#section02" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/tmap.jpg" width="298" height="213" alt="アクセスマップ"></a></li>
            <li>
              <div class="fb-page pc" data-href="https://www.facebook.com/%E6%9D%BE%E6%9C%AC%E9%9B%BB%E6%A9%9F%E5%B7%A5%E6%A5%AD%E6%89%80-386759728172780/" data-tabs="event" data-width="298" data-height="213" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/%E6%9D%BE%E6%9C%AC%E9%9B%BB%E6%A9%9F%E5%B7%A5%E6%A5%AD%E6%89%80-386759728172780/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/%E6%9D%BE%E6%9C%AC%E9%9B%BB%E6%A9%9F%E5%B7%A5%E6%A5%AD%E6%89%80-386759728172780/">松本電機工業所</a></blockquote></div>
            </li>
          </ul>

          <div class="fb_wrap rsp">
            <div class="fb-page rsp" data-href="https://www.facebook.com/%E6%9D%BE%E6%9C%AC%E9%9B%BB%E6%A9%9F%E5%B7%A5%E6%A5%AD%E6%89%80-386759728172780/" data-tabs="event" data-width="500" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/%E6%9D%BE%E6%9C%AC%E9%9B%BB%E6%A9%9F%E5%B7%A5%E6%A5%AD%E6%89%80-386759728172780/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/%E6%9D%BE%E6%9C%AC%E9%9B%BB%E6%A9%9F%E5%B7%A5%E6%A5%AD%E6%89%80-386759728172780/">松本電機工業所</a></blockquote></div>
          </div><!-- /.fb_wrap -->
        </div><!-- /.inner -->
      </div><!-- /#section03 -->
    </main>

<?php get_footer(); ?>