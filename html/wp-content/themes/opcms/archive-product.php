<?php $this_pagetitle = get_post_type_object(get_post_type())->label; ?>
<?php $this_archive_title = get_post_type_object(get_post_type())->name; ?>
<?php get_header(); ?>

    <main id="product" class="low_main">
      <div class="inner">
        <section id="section01" class="section last">
          <h3 class="h3ttl01"><img src="<?php echo home_url('/'); ?>assets/img/product/h3ttl01.gif" width="419" height="29" alt="高度な技術に裏打ちされた修理"></h3>
          <p class="txt">当社のモーター・ポンプ類の修理工程例をご紹介します。</p>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
          <section class="unit">
            <dl>
              <dt class="bold">CASE：</dt>
              <dd>
                <h4 class="bold"><?php the_title(); ?></h4>
              </dd>
            </dl>
            <ul>
              <li>
                <p class="img">
<?php
  $attachment_id = get_field('product_img01');
  $size = "product_custom_size";
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  $attachment = get_post( get_field('product_img01') );
  $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
  $image_title = $attachment->post_title;
?>
                  <img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php the_title(); ?>" /></p>
                <p>１．<?php the_field('product_cmt01'); ?></p>
              </li>
              <li>
                <p class="img">
<?php
  $attachment_id = get_field('product_img02');
  $size = "product_custom_size";
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  $attachment = get_post( get_field('product_img02') );
  $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
  $image_title = $attachment->post_title;
?>
                  <img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php the_title(); ?>" />
                </p>
                <p>２．<?php the_field('product_cmt02'); ?></p>
              </li>
              <li>
                <p class="img">
<?php
  $attachment_id = get_field('product_img03');
  $size = "product_custom_size";
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  $attachment = get_post( get_field('product_img03') );
  $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
  $image_title = $attachment->post_title;
?>
                  <img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php the_title(); ?>" />
                </p>
                <p>３．<?php the_field('product_cmt03'); ?></p>
              </li>
              <li>
                <p class="img">
<?php
  $attachment_id = get_field('product_img04');
  $size = "product_custom_size";
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  $attachment = get_post( get_field('product_img04') );
  $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
  $image_title = $attachment->post_title;
?>
                  <img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php the_title(); ?>" />
                </p>
                <p>４．<?php the_field('product_cmt04'); ?></p>
              </li>
              <li>
                <p class="img">
<?php
  $attachment_id = get_field('product_img05');
  $size = "product_custom_size";
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  $attachment = get_post( get_field('product_img05') );
  $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
  $image_title = $attachment->post_title;
?>
                  <img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php the_title(); ?>" />
                </p>
                <p>５．<?php the_field('product_cmt05'); ?></p>
              </li>
<?php $img_id = get_field('product_img06'); ?>
<?php if(empty($img_id)):?>
<?php else: ?>
              <li>
                <p class="img">
<?php
  $attachment_id = get_field('product_img06');
  $size = "product_custom_size";
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  $attachment = get_post( get_field('product_img06') );
  $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
  $image_title = $attachment->post_title;
?>
                  <img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php the_title(); ?>" />
                </p>
                <p>６．<?php the_field('product_cmt06'); ?></p>
              </li>
<?php endif; ?>

<?php $img_id = get_field('product_img07'); ?>
<?php if(empty($img_id)):?>
<?php else: ?>
              <li>
                <p class="img">
<?php
  $attachment_id = get_field('product_img07');
  $size = "product_custom_size";
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  $attachment = get_post( get_field('product_img07') );
  $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
  $image_title = $attachment->post_title;
?>
                  <img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php the_title(); ?>" />
                </p>
                <p>7．<?php the_field('product_cmt07'); ?></p>
              </li>
<?php endif; ?>

<?php $img_id = get_field('product_img08'); ?>
<?php if(empty($img_id)):?>
<?php else: ?>
              <li>
                <p class="img">
<?php
  $attachment_id = get_field('product_img08');
  $size = "product_custom_size";
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  $attachment = get_post( get_field('product_img08') );
  $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
  $image_title = $attachment->post_title;
?>
                  <img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php the_title(); ?>" />
                </p>
                <p>8．<?php the_field('product_cmt08'); ?></p>
              </li>
<?php endif; ?>

<?php $img_id = get_field('product_img09'); ?>
<?php if(empty($img_id)):?>
<?php else: ?>
              <li>
                <p class="img">
<?php
  $attachment_id = get_field('product_img09');
  $size = "product_custom_size";
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  $attachment = get_post( get_field('product_img09') );
  $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
  $image_title = $attachment->post_title;
?>
                  <img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php the_title(); ?>" />
                </p>
                <p>9．<?php the_field('product_cmt09'); ?></p>
              </li>
<?php endif; ?>
            </ul>
          </section><!-- /.unit -->
<?php endwhile; endif; ?>
<?php
  if (function_exists("pagination")) {
    pagination($additional_loop->max_num_pages);
  }
?>
        </section><!-- /#section01 -->

        <div id="bnr_area" class="pc">
          <div class="bnr">
            <p class="txt01 left">各種電動機のメンテナンスのプロフェッショナルです</p>
            <div class="txt_area01 right">
              <p class="txt02">修理のお見積りやご質問などお気軽にお問い合わせください。</p>
              <p class="txt03 bold"><span>Tel.</span>028-658-1649  <span>Fax.</span>028-659-2299</p>
            </div><!-- /txt_area -->
          </div><!-- /.bnr -->
          <div class="txt_area02">
            <p>種モーター・水中ポンプのメンテナンス・修理・販売ならおまかせ！</p>
            <p>各種モーター巻替、モーター回転制御改造、水中ポンプのメンテナンス・修理・販売を行っております。また高圧モーター等の特殊なものにも対応しております。取扱製品一覧に掲載されている製品は一例です。幅広く対応させて頂きますので、お気軽にお問合せ下さい。</p>
          </div><!-- /.txt_area02 -->
        </div><!-- /#bnr_area -->
      </div><!-- /.inner -->
    </main>

<?php get_footer(); ?>