<?php $this_pagetitle = get_post_type_object(get_post_type())->label; ?>
<?php $this_archive_title = get_post_type_object(get_post_type())->name; ?>
<?php get_header(); ?>

    <main id="news" class="low_main">
      <div class="inner">
        <div class="artcle_area left">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
  <?php $postid = get_the_ID(); ?>
          <article class="article">
            <h3 id="post<?php echo $postid; ?>" class="h3ttl bold"><?php the_title(); ?></h3>
            <p class="date">投稿日 <?php the_time("Y年n月j日"); ?></p>
            <?php the_content(); ?>
          </article>
<?php endwhile; endif; ?>
<?php
  if (function_exists("pagination")) {
    pagination($additional_loop->max_num_pages);
  }
?>
        </div><!-- /.artcle_area -->

<?php get_sidebar(); ?>

      </div><!-- /.inner -->
    </main>


<?php get_footer(); ?>