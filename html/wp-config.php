<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'aa177dzhum_wordpress');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'aa177dzhum');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'bgd3r7RT');

/** MySQL のホスト名 */
define('DB_HOST', '127.0.0.1');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8{]2^]@91aEApKb@h`1t|=K7!gg+geE1pOXE(Yk^2.%Q8~bWk(iws*oi.(f1^g5,');
define('SECURE_AUTH_KEY',  '+jHutTl$ b#[c 5EKgs%ri^&v{rbf+LGrYxyMt}}*2)-2J{s)`Z|]f?O/?Az6P -');
define('LOGGED_IN_KEY',    'u7.+c-&y(R;=GN&Jy~xPc;w6Gg*-%k-{$+Y5pK5izwk 2/VWj81:?jd(~<@/MipC');
define('NONCE_KEY',        'BM$OFN:&})+ V~*31U4>$KArf]7*4h@OHz8Qt7Fu,xL-ul=^|q-L@ApTrQ(@ RO7');
define('AUTH_SALT',        'UVv=k~;ph5-r^NwUyK`v%pUOk;cpr,JiFi;Yt<|gvLc/|uN- ^T/AS#=`O-S#J:f');
define('SECURE_AUTH_SALT', '$-ni{P`pbdC(_m[3YXx+@X{`fV &zapDg_q>CJ}r#Pi5>p#8LZV@O#> TKLvRi(X');
define('LOGGED_IN_SALT',   'qX+^|Ky/[-A~f}-j6s<v$2=;5n`J{]/2%n+Qzwq4|gB0ag!R!P7sE-#,b4$+}SKu');
define('NONCE_SALT',       'R>s]Syy{awp[*>7Ei-O_s2|To0DWO{h^Z9@RfjNy8:1ySKc*Rx4-]7fKy*LO>TgG');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
